SPDX-FileCopyrightText: 2023 Martin Knobel aka Chipperdoodles <chipperdoodles@protonmail.com>
SPDX-License-Identifier: CERN-OHL-S-2.0+

 ------------------------------------------------------------------------------
| Copyright Martin Knobel (aka chipperdoodles) 2023.                           |
|                                                                              |
| This source describes Open Hardware and is licensed under the CERN-OHL-S v2. |
|                                                                              |
| You may redistribute and modify this source and make products using it under |
| the terms of the CERN-OHL-S v2 (https://ohwr.org/cern_ohl_s_v2.txt).         |
|                                                                              |
| This source is distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,          |
| INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A         |
| PARTICULAR PURPOSE. Please see the CERN-OHL-S v2 for applicable conditions.  |
|                                                                              |
| Source location: https://gitlab.com/chippydootles/chippydootles-hardware     |
|                                                                              |
| As per CERN-OHL-S v2 section 4, should You produce hardware based on this    |
| source, You must where practicable maintain the Source Location visible      |
| on the external case of the Gizmo or other products you make using this      |
| source.                                                                      |
 ------------------------------------------------------------------------------

A full copy of the CERN-OHL-S v2 can be found in the License Folder